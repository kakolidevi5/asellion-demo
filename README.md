### Environment Setup
- Install Java8 [Oracle](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- Install postgresql [PostgreSQL](https://www.postgresql.org/download/)
  - Install and Connect to postgresql via pgAdmin, enter port as `5432` to match the one in `application.yml` and create a 	user with username and password found in the `application.yml` file
  - Also create the database with name of `demoDB` found in the `application.yml` file
- Install maven [Maven](https://maven.apache.org/download.cgi) ( > 3.2 )
- To use the `application.yml`, make sure your environment variable `SPRING_PROFILES_ACTIVE` is set to `default`
  or pass `-Dspring.profiles.active=default` as one of the `mvn` parameter

#### BootRun

```
mvn spring-boot:run
```

#### Or Alternatively Build and Run

```
mvn clean package
java -jar target/<artifact-name>.jar
```

### Localhost API Endpoint
```
http://localhost:8083/productapi
```

### ngrok tunneled API Endpoint
To test accessing my server (tunneled with ngrok), 

```
1. http://b9b9a628.ngrok.io/productapi --> If my server is up and this endpoint is not expired yet.
2. Or remind me to turn on my server so that you can access it using a new ngrok endpoint.
```