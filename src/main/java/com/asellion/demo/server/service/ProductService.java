package com.asellion.demo.server.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asellion.demo.server.controller.ProductController;
import com.asellion.demo.server.model.Product;
import com.asellion.demo.server.repository.ProductRepository;
import com.asellion.demo.server.utilities.ProductRequest;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

	public List<Product> getAllProdutsList() throws Exception {
		List<Product> productsList = productRepository.findAll();
		return productsList;
	}

	public Optional<Product> getProductByName(String productName) throws Exception {
		Optional<Product> productOptional = productRepository.findProductByName(productName);
		return productOptional;
	}

	public Product createProduct(ProductRequest productPayload) {
		try {
			Product product = new Product();
			product.setName(productPayload.getName());
			product.setCurrentPrice(productPayload.getCurrentPrice());
			product.setDescription(productPayload.getDescription());
			product = productRepository.save(product);
			return product;

		} catch (Exception e) {
			LOGGER.error("Exception = {}", e);
		}
		return null;
	}

	public Product updateProduct(Product product, ProductRequest productPayload) throws Exception {
		product.setName(productPayload.getName());
		product.setCurrentPrice(productPayload.getCurrentPrice());
		product.setDescription(productPayload.getDescription());
		product = productRepository.save(product);
		return product;
	}
}