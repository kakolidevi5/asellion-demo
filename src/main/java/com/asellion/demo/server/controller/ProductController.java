package com.asellion.demo.server.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.asellion.demo.server.model.Product;
import com.asellion.demo.server.service.ProductService;
import com.asellion.demo.server.utilities.ProductRequest;

@RestController
@RequestMapping("/product")
public class ProductController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	private ProductService productUtility;

	@RequestMapping(value = "/getAllProducts", method = RequestMethod.GET)
	@PreAuthorize("hasAnyRole('ADMIN')")
	@Transactional
	public ModelAndView getListOfProducts(Model model) {

		ModelAndView mav = null;
		try {
			List<Product> productsList = productUtility.getAllProdutsList();

			mav = new ModelAndView("getAllProducts");
			mav.addObject("productList", productsList);
			mav.setStatus(HttpStatus.OK);
			return mav;

		} catch (Exception e) {
			LOGGER.error("Exception = {}", e);
			mav = new ModelAndView("error");
			mav.setStatus(HttpStatus.BAD_GATEWAY);
			return mav;
		}
	}

	@RequestMapping(value = "/getOneProduct", method = RequestMethod.GET)
	@PreAuthorize("hasAnyRole('ADMIN')")
	@Transactional
	ModelAndView getAProductFromList(@RequestParam(value = "product_name", required = true) String productName,
			Model model) {

		ModelAndView mav = null;
		try {
			Product product = null;
			Optional<Product> productOptional = productUtility.getProductByName(productName);

			if (productOptional.isPresent()) {
				product = productOptional.get();
			}
			mav = new ModelAndView("getOneProduct");
			mav.addObject("product", product);
			mav.setStatus(HttpStatus.OK);
			return mav;

		} catch (Exception e) {
			LOGGER.error("Exception = {}", e);
			mav = new ModelAndView("error");
			mav.setStatus(HttpStatus.BAD_GATEWAY);
			return mav;
		}
	}

	@RequestMapping(value = "/updateProduct", method = RequestMethod.POST)
	@PreAuthorize("hasAnyRole('ADMIN')")
	@Transactional
	ModelAndView updateAProduct(@ModelAttribute @Valid ProductRequest productPayload) {

		ModelAndView mav = null;
		try {
			Product product = null;
			Optional<Product> productOptional = productUtility.getProductByName(productPayload.getName());

			if (productOptional.isPresent()) {
				product = productUtility.updateProduct(productOptional.get(), productPayload);
			}
			mav = new ModelAndView("updateProduct");
			mav.addObject("product", product);
			mav.setStatus(HttpStatus.OK);
			return mav;

		} catch (Exception e) {
			LOGGER.error("Exception = {}", e);
			mav = new ModelAndView("error");
			mav.setStatus(HttpStatus.BAD_GATEWAY);
			return mav;
		}
	}

	@RequestMapping(value = "/createProduct", method = RequestMethod.POST)
	@PreAuthorize("hasAnyRole('ADMIN')")
	@Transactional
	ModelAndView createAProduct(@ModelAttribute @Valid ProductRequest productPayload) {

		ModelAndView mav = null;
		try {
			Product product = productUtility.createProduct(productPayload);
			mav = new ModelAndView("createProduct");
			mav.addObject("product", product);
			mav.setStatus(HttpStatus.OK);
			return mav;

		} catch (Exception e) {
			LOGGER.error("Exception = {}", e);
			mav = new ModelAndView("error");
			mav.setStatus(HttpStatus.BAD_GATEWAY);
			return mav;
		}
	}

	@RequestMapping(value = "/updateForm", method = RequestMethod.GET)
	@PreAuthorize("hasAnyRole('ADMIN')")
	ModelAndView updateForm() {
		return new ModelAndView("updateForm");
	}

	@RequestMapping(value = "/createForm", method = RequestMethod.GET)
	@PreAuthorize("hasAnyRole('ADMIN')")
	ModelAndView createForm() {
		return new ModelAndView("createForm");
	}

	@RequestMapping(value = "/getForm", method = RequestMethod.GET)
	@PreAuthorize("hasAnyRole('ADMIN')")
	ModelAndView getForm() {
		return new ModelAndView("getForm");
	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	@PreAuthorize("hasAnyRole('ADMIN')")
	ModelAndView home() {
		return new ModelAndView("home");
	}
}