package com.asellion.demo.server.utilities;

import javax.validation.constraints.NotBlank;

public class ProductRequest {

	private Long id;

	@NotBlank
	private String name;

	private String currentPrice;

	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(String currentPrice) {
		this.currentPrice = currentPrice;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Product [name=" + name + ", currentPrice=" + currentPrice + ", description=" + description + "]";
	}
}
